create database items default character set utf8;
use items;
create table item_category(
	category_id int primary key not null auto_increment,
	category_name varchar(256) not null);
create table item(
	item_id int primary key not null auto_increment,
	item_name varchar(256)not null,
	item_price int not null,
	category_id int);
